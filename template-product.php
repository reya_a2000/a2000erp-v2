<?php

/**
 * Template Name: Product
 *
 */

get_header();
?>
<div class="container-fluid px-0">
<h1>TEST</h1>
<!-- Product pages generic sections -->
<?php
$arFppArgs = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'category_name' => 'For Product Pages',
            );

$arPosts = new WP_Query( $arFppArgs );
 
if ( $arPosts->have_posts() ) :
 
    while ( $arPosts->have_posts() ) :
        $arPosts->the_post();
        
        if( $post->post_name == 'product-with-icon' ) :
            if( have_rows('prod_w_icon') ): 
                /**
                 * if bg image is available --- check if has overlay and apply;
                 * if bg image is unavailable and bg color is available --- apply bg color  
                 */
                $strBg =  'transparent';

                if ( get_field('pwi_bg_img') != '' ) {
                    $strBg = 'url('.get_field('pwi_bg_img').')';

                    if ( get_field('pwi_img_overlay') != '' ) {
                        $strBg = 'linear-gradient('.get_field('pwi_img_overlay').', '.get_field('pwi_img_overlay').'), '.$strBg;
                    }
                } else if ( get_field('pwi_bg_color') != '' ) {
                    $strBg = get_field('pwi_bg_color');
                } ?>
                <section class="prod-w-icon pt-md-5 pt-4" style="background: <?php echo $strBg ?> no-repeat center/cover">
                    <h1 class="text-center" style="color: <?php echo get_field('pwi_txt_color') != '' ? get_field('pwi_txt_color') : '#000' ?>">
                        <?php the_title(); ?>
                    </h1>
                    <hr class="h1-underline" style="border-bottom-color: <?php echo get_field('pwi_hr_color') != '' ? get_field('pwi_hr_color') : acf_get_field('pwi_hr_color')['default_value'] ?>"/>
                    <div class="row container mx-auto">
<?php            
                    while( have_rows('prod_w_icon') ):
                        the_row(); ?>
                        <div class="col-md-3 col-6 px-1 px-lg-3 my-2 my-lg-4 py-4">
                            <a href="<?php the_sub_field('prod_url') ?>">
                                <img src="<?php the_sub_field('prod_icon'); ?>" class="d-flex mx-auto icon"/>
                                <p class="text-center mt-2" style="color: <?php echo get_field('pwi_txt_color') != '' ? get_field('pwi_txt_color') : '#000' ?>">
                                    <?php the_sub_field('prod_name') ?>
                                </p>
                            </a>
                        </div>
                
<?php
                    endwhile; ?>
                    </div>
                </section>
<?php
            endif;
        endif;

    endwhile;

endif; ?>
</div>
<?php get_footer(); ?>