<nav class="navbar navbar-expand-lg navbar-light bg-light a2000-contact-info">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primaryNavMenu" aria-controls="primaryNavMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand mx-auto pl-md-5" href="#">
        <img src="<?php echo get_theme_mod('headInfoNavSetting-logo', get_template_directory_uri().'/screenshot.png') ?>" id="logo-info">
    </a>
    <div class="collapse navbar-collapse ml-md-5" id="headinfoSupportedContent">
        <div class="row mx-md-auto">
            <div class="col-md-4 my-auto">
                <div class="d-flex">
                    <i class="fas fa-map-marker-alt fa-2x my-auto"></i>
                    <div class="d-flex flex-column ml-3">
                        <span>LOCATION</span>
                        <span id="location-info">
                            <?php echo get_theme_mod('headInfoNavSetting-location', 'AMK Tech II, Singapore | Puchong Selangor Darul Ehsan, Malaysia') ?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-auto">
                <div class="d-flex">
                    <i class="fas fa-envelope fa-2x my-auto"></i>
                    <div class="d-flex flex-column ml-3">
                        <span>EMAIL</span>
                        <span id="email-info">
                            <a href="mailto:<?php echo get_theme_mod('headInfoNavSetting-email', 'info@a2000.net') ?>">
                                <?php echo get_theme_mod('headInfoNavSetting-email', 'info@a2000.net') ?>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-auto">
                <div class="d-flex">
                    <i class="fas fa-phone-alt fa-2x my-auto"></i>
                    <div class="d-flex flex-column ml-3">
                        <span>PHONE</span>
                        <span id="phone-info">
                            <?php echo get_theme_mod('headInfoNavSetting-phone', '+65 6720 2000') ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg primary-main-menu py-0">
    <?php
        wp_nav_menu( array(  
                'theme_location'  => 'primary',
                'container'       => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id'    => 'primaryNavMenu',  
                'items_wrap'      => '<ul class="navbar-nav mx-auto">%3$s</ul>',
                'add_li_class'    => 'nav-item py-2 py-auto',
                'add_a_class'     => 'nav-link'
            ) 
        );
    ?>
</nav>
<div id="headInfoSubmenu" class="navbar d-none d-lg-none pb-lg-4">
</div>

