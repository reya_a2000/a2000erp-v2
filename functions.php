<?php
/**
 * Load Styles and Scripts
 */
function loadStylesScripts() {
    wp_enqueue_style( 'a2000erp-css', get_stylesheet_uri() );

    // call for vendors and other custom css 
    $strAssetUrl = get_template_directory_uri().'/assets/';

    wp_enqueue_style( 'bootstrap-css', $strAssetUrl.'vendors/bootstrap-4.0.0/css/bootstrap.min.css');
    wp_enqueue_style( 'fontawesome-css', $strAssetUrl.'vendors/fontawesome-5.14.0/css/all.min.css');

    // js
    wp_enqueue_script( 'jquery-js', $strAssetUrl.'js/jquery-3.5.1.slim.min.js');
    wp_enqueue_script( 'bootstrap-js', $strAssetUrl.'vendors/bootstrap-4.0.0/js/bootstrap.min.js');
    wp_enqueue_script( 'matchHeight-js', $strAssetUrl.'vendors/jquery.matchHeight.js');
    wp_enqueue_script( 'lazysizes-js', $strAssetUrl.'vendors/lazysizes.min.js');
    wp_enqueue_script( 'a2000custom-js', $strAssetUrl.'js/a2000script.js');
}

add_action( 'wp_enqueue_scripts', 'loadStylesScripts' );

// removes html margin-top: 32px
show_admin_bar( false );

/**
 * Register Menus
 */
function registerMenus(){
	register_nav_menus( array(
            'primary'   => __( 'Primary', 'a2000erp-v2' ),
            'header-li' => __( 'Header LI Menu', 'a2000erp-v2' ),
            'mobile-li' => __( 'Mobile LI Menu', 'a2000erp-v2' ),
            'footer'    => __( 'Footer Menu', 'a2000erp-v2' ),
        ) 
    );
  
	add_theme_support( 'post-thumbnails');
} 

add_action( 'after_setup_theme', 'registerMenus' );

function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }

    return $classes;
}

add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);
 
function add_additional_class_on_a( $atts, $item, $args ) {
    if(isset($args->add_li_class)) { 
        $atts['class'] = $args->add_a_class;
    }
    
    return $atts;
}

add_filter( 'nav_menu_link_attributes', 'add_additional_class_on_a', 10, 3 );


require_once get_parent_theme_file_path("/assets/customizer/core.php"); 
?>