(function($) {
    $(document).ready(function() {
        

        $(window).on("scroll", function() {
            var contactInfoHeight = $('.a2000-contact-info').innerHeight();
            
            if ($(window).width() >= 992) {

                
                if ( $(window).scrollTop() > contactInfoHeight) {
                    $('.primary-main-menu').addClass('position-fixed w-100');
                    $('.primary-main-menu').css('top','0');
                    // sub-menu
                    $('#headInfoSubmenu').css('top','56px');
                } else {
                    $('.primary-main-menu').removeClass('position-fixed w-100');
                    // sub-menu
                    var topTotal = contactInfoHeight + 56;
                    $('#headInfoSubmenu').css('top',topTotal+'px');
                }
            } else {
                $('.a2000-contact-info').addClass('position-fixed w-100');
                $('.primary-main-menu').addClass('position-fixed w-100');
                $('.primary-main-menu').css('top',contactInfoHeight);
            }
            
        })

        $(window).on("load resize", function() {
            $("#primaryNavMenu .navbar-nav > .menu-item-has-children > .sub-menu").css("display", "none");

            /**
         * mouse enter and mouse leave primary menu
         */

        var mouseEnterCnt = 0;

        $("#primaryNavMenu .navbar-nav > .menu-item-has-children > a").click(function(e) { 
            e.preventDefault();

            if ($(window).width() < 992) {
                $("#primaryNavMenu .navbar-nav > .menu-item-has-children > .sub-menu").css("display", "none");
                $(this).parent().find("> .sub-menu").css("display", "block");
            }
            
        })

        jQuery(document).on('click', '#headInfoSubmenu > ul.sub-menu > li.menu-item-has-children > a.nav-link, #primaryNavMenu .navbar-nav > .menu-item-has-children > .sub-menu > li > a', function(e) {
            e.preventDefault();
        })

        if ($(window).width() >= 992) {

            $("#primaryNavMenu .navbar-nav > .menu-item-has-children, #headInfoSubmenu").mouseenter(function(e) {
                mouseEnterCnt = 0;
                var leftPos = $(this).position().left;
    
                console.log(leftPos)
                mouseEnterCnt++;

                leftPos = leftPos > 200 ? leftPos-100 : leftPos;

                $("#headInfoSubmenu").html('<ul class="sub-menu px-0">'+$(this).find("> .sub-menu").html());

                var isMultiSubmenu = false;
                // for sub menu with children
                if ($(this).find(".menu-item-has-children").length > 0) {
                    $("#headInfoSubmenu > ul.sub-menu > li").css("float", "left");

                    isMultiSubmenu = true;
                } else {
                    leftPos = leftPos+100;
                }

                if ($(this).hasClass('menu-item')) {
                    var subMenuLen = $("#headInfoSubmenu > ul").find("> li").length;

                    leftPos = subMenuLen >= 5 && isMultiSubmenu ? leftPos-100 : leftPos;

                    console.log(leftPos)
                    $("#headInfoSubmenu").css('left',leftPos)
                }

                

                $("#headInfoSubmenu").removeClass('d-lg-none').addClass('d-lg-block')

                $('#headInfoSubmenu > ul.sub-menu > li').matchHeight();
                $('#headInfoSubmenu > ul.sub-menu > li.menu-item > a.nav-link').matchHeight();
            }).mouseleave(function() {
                mouseEnterCnt--;
                if (!mouseEnterCnt) {
                    $("#headInfoSubmenu").removeClass('d-lg-block').addClass('d-lg-none');
                }
                    
            })
        }
        })


        
        
       
    });
})(jQuery);