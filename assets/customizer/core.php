<?php

/**
 * Header menu customization
 */

function headInfoNav($wp_customize) {
    $strSectionName = "headInfoNavSection";
    $strSettingName = "headInfoNavSetting";
    $strControlName = "headInfoNavControl";

    $wp_customize->add_section( $strSectionName,array(
        'title'         => __('Head Info Nav', 'a2000erp-v2'), 
        'description'   => __('Edit page', 'a2000erp-v2'), 
    ));
    
    // Logo
    $wp_customize->add_setting($strSettingName.'-logo', array(
        "transport" => "postMessage"
    ));

    $wp_customize->add_control(
        new WP_Customize_Image_Control($wp_customize, $strControlName.'-logo', array(
                "label"         => __('Logo', 'a2000erp-v2'),
                "section"       => $strSectionName,
                "settings"      => $strSettingName.'-logo'
            )
        )
    );

    $wp_customize->selective_refresh->add_partial($strSettingName.'-logo', array(
        "selector"          => "#logo-info",
        "render_callback"   =>  function() {
                                    return get_theme_mod($strSettingName.'-logo');
                                }
    ));

    // Location
    $wp_customize->add_setting($strSettingName.'-location', array(
        'default'   => "AMK Tech II, Singapore | Puchong Selangor Darul Ehsan, Malaysia",
        'transport' => 'postMessage' 
    ));

    $wp_customize->add_control(
        new WP_Customize_Control($wp_customize, $strControlName.'-location', array(
                'label'             => __('Location', 'a2000erp-v2'),
                'type'              => 'textarea',
                'section'           => $strSectionName,
                'settings'          => $strSettingName.'-location',
                'sanitize_callback' => 'wp_filter_nohtml_kses',
            )
        )
    );

    $wp_customize->selective_refresh->add_partial($strSettingName.'-location', array(
        "selector"          => "#location-info",
        "render_callback"   =>  function() {
                                    return get_theme_mod($strSettingName.'-location');
                                }
    ));

    // Email
    $wp_customize->add_setting($strSettingName.'-email', array(
        'default'   => "info@a2000.net",
        'transport' => 'postMessage' 
    ));

    $wp_customize->add_control(
        new WP_Customize_Control($wp_customize, $strControlName.'-email', array(
                'label'             => __('Email', 'a2000erp-v2'),
                'type'              => 'text',
                'section'           => $strSectionName,
                'settings'          => $strSettingName.'-email',
            )
        )
    );

    $wp_customize->selective_refresh->add_partial($strSettingName.'-email', array(
        "selector"          => "#email-info",
        "render_callback"   =>  function() {
                                    return get_theme_mod($strSettingName.'-email');
                                }
    ));

    // Phone
    $wp_customize->add_setting($strSettingName.'-phone', array(
        'default'   => "+65 6720 2000",
        'transport' => 'postMessage' 
    ));

    $wp_customize->add_control(
        new WP_Customize_Control($wp_customize, $strControlName.'-phone', array(
                'label'             => __('Phone', 'a2000erp-v2'),
                'type'              => 'text',
                'section'           => $strSectionName,
                'settings'          => $strSettingName.'-phone',
            )
        )
    );

    $wp_customize->selective_refresh->add_partial($strSettingName.'-phone', array(
        "selector"          => "#phone-info",
        "render_callback"   =>  function() {
                                    return get_theme_mod($strSettingName.'-phone');
                                }
    ));

}

add_action( 'customize_register', 'headInfoNav' );

/**
 * Footer customization
 */

function footerCustom($wp_customize) {
    
    $wp_customize->add_section( "footerSection",array(
        'title'         => __('Footer', 'a2000erp-v2'), 
        'description'   => __('Edit page', 'a2000erp-v2'), 
    ));

    $wp_customize->add_setting("footerAboutSetting", array(
        'default'   => "",
        'transport' => 'postMessage' 
    ));

    $wp_customize->add_control(
        new WP_Customize_Control($wp_customize, "footerAboutControl", array(
                'label'             => __('About', 'a2000erp-v2'),
                'type'              => 'textarea',
                'section'           => "footerSection",
                'settings'          => "footerAboutSetting",
            )
        )
    );

    $wp_customize->selective_refresh->add_partial("footerAboutSetting", array(
        "selector"          => "",
        "render_callback"   =>  function() {
                                    return get_theme_mod("footerAboutSetting");
                                }
    ));
}

add_action( 'customize_register', 'footerCustom' );
?>